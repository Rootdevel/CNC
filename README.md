# First prototype of Computer Numeric Control (CNC).
======

- 3d model designed in freecad
- Aluminum profiles


Code | Firmware
======

Docs
======

Model 3D (freecad)
======

PCB (Kicad)
======

![3D_CNC_Animation](/Model3D/Animation/CNC-Animation.gif)
![3D_CNC](/images/CNC.png)
![3D_CNC-EjeY](/images/EjeY.png)
![3D_CNC-EjeX](/images/EjeX.png)
![3D_CNC-EjeZ](/images/EjeZ.png)

License 
======

First prototype of Computer Numeric Control (CNC) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

Attributions
============
- @fandres323
- @maurinc2010
